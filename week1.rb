def Swap(num1,num2)
	tmp = num1
	num1 = num2
	num2 = tmp
end

def BCNN(num1,num2)
	if(num1 > num2)
		Swap(num1,num2)
	end
	i = num2
	while i>= num2
		if(i % num1 == 0 && i % num2 == 0)
			return i
		end
		i += 1
	end
end

def UCLN(num1,num2)
	i = BCNN(num1,num2)
	return num1 * num2 / i
end

def Ktra_So_nguyen_to(num)
	if( num == 1 || num == 2)
		return true
	end
	i = 2
	while i < num 
		if(num % i == 0)
			return "khong la so nguyen to"
		end
		i += 1
	end
	return "la so nguyen to"
end
print "Tim UCLN va BCNN:\n "
print "\tNhap so thu nhat: "
num1 = gets
num1.chomp
num1 = num1.to_i
print "\tNhap so thu hai: "
num2 = gets
num2.chomp
num2 = num2.to_i
print "\tUCLN(#{num1},#{num2}) = #{UCLN(num1,num2)}\n"
print "\tBCNN(#{num1},#{num2}) = #{BCNN(num1,num2)}\n"
print "Kiem tra so nguyen to:\n\tNhap so: "
num3 = gets
num3.chomp
num3 = num3.to_i
print "\tKet qua: #{Ktra_So_nguyen_to(num3)}\n"